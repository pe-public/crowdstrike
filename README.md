This module has been moved to Puppet Forge as
[ruthenium-crowdstrike](https://forge.puppet.com/modules/ruthenium/crowdstrike).
